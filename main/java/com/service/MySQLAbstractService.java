package com.service;

import com.dbConnector.MySQLConnectorManager;
import com.entity.AbstractEntity;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.utils.UtilConstants.*;

/**
 * Created by Serg on 23.09.2018.
 */
public abstract class MySQLAbstractService {

    private static final Logger LOGGER = LogManager.getLogger(MySQLAbstractService.class);


    public void updateItem(AbstractEntity entity, String query) throws SQLException {

        Connection connection = MySQLConnectorManager.getConnection();

        MySQLConnectorManager.startTransaction(connection);

        PreparedStatement statement = connection.prepareStatement(query);

        switch (entity.getClassName()) {

            case "Station": {
                try {
                    statement.setString(1, entity.getStationName());
                    statement.setInt(2, entity.getId());
                    statement.executeUpdate();

                } catch (SQLException e) {
                    throw new SQLException(STATION_EXISTS);
                }
                break;
            }
            case "User": {
                break;
            }

            default: {
                MySQLConnectorManager.rollbackTransaction(connection);
                MySQLConnectorManager.closeConnection(connection);

                throw new SQLException(UNKNOWN_ENTITY);
            }
        }

        MySQLConnectorManager.commitTransaction(connection);
    }


    public void addNewItem(AbstractEntity entity, String query) throws SQLException {

        Connection connection = MySQLConnectorManager.getConnection();

        MySQLConnectorManager.startTransaction(connection);

        PreparedStatement statement = connection.prepareStatement(query);

        switch (entity.getClassName()) {

            case USER : {
                try {
                    statement.setString(1, entity.getFirstName());
                    statement.setString(2, entity.getLastName());
                    statement.setString(3, entity.getEmail());
                    statement.setString(4, entity.getPassword());
                    statement.setBoolean(5, false);
                    statement.executeUpdate();

                } catch (SQLException e) {

                    throw new SQLException(USER_EXISTS);
                }

                break;
            }
            case ROUTE : {
                try{
                    statement.setString(1, entity.getCode());
                    statement.setInt(2, entity.getStartStationId());
                    statement.setString(3, entity.getDepartureTime().toString());
                    statement.setInt(4, entity.getFinishStationId());
                    statement.setString(5, entity.getArrivalTime().toString());

                    statement.executeUpdate();

                }catch (SQLException e){
                    throw new SQLException(ROUTE_EXISTS);
                }
                break;
            }
            case STATION : {
                try {
                    statement.setString(1, entity.getStationName());

                    statement.executeUpdate();

                } catch (SQLException e) {

                    throw new SQLException(STATION_EXISTS);
                }
                break;
            }
            case INTER_STATION : {
                try{
                    statement.setInt(1, entity.getId());
                    statement.setInt(2, entity.getStartStationId());
                    statement.setString(3, entity.getArrivalTime().toString());
                    statement.setString(4, entity.getDepartureTime().toString());

                    statement.executeUpdate();
                } catch (SQLException e) {

                    throw new SQLException(INTERMEDIATE_STATION_ERROR);
                }

                break;
            }
            default: {
                MySQLConnectorManager.rollbackTransaction(connection);
                MySQLConnectorManager.closeConnection(connection);

                throw new SQLException(UNKNOWN_ENTITY);
            }
        }
        MySQLConnectorManager.commitTransaction(connection);

    }


    public void deleteItem(int itemId, String query) throws SQLException {

        Connection connection = MySQLConnectorManager.getConnection();

        MySQLConnectorManager.startTransaction(connection);

        PreparedStatement statement = connection.prepareStatement(query);

        statement.setInt(1, itemId);

        statement.executeUpdate();

        MySQLConnectorManager.commitTransaction(connection);


    }

}
