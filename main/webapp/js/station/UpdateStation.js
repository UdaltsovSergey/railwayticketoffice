var station = new Object();
var vocabulary;
var language;

$(window).ready(function () {

    vocabulary = getVocabulary();

    language = JSON.parse(window.localStorage.getItem('lang'));

    if (language !== null && language !== undefined) {
        translatePage(language);
    }

    var stationId = window.location.href.split("?")[1].split("=")[1];

    loadStationInfo(stationId);

    $("#saveStation").click(function () {

        station.id = stationId;
        station.name = $("#nStation").val();

        (station.name === "")
            ? alert(vocabulary[language]['fillUp'])
            : updateStation(JSON.stringify(station));


    });



    $("#cancelButton").click(function () {

        window.history.back();

    });

    $('.translate').click(function () {
        var transLang = $(this).attr('id');

        translatePage(transLang);

        window.localStorage.setItem('lang', JSON.stringify(transLang));
    });

});

function loadStationInfo(stationId) {

    console.log('load start')
    $.ajax({
        type: 'get',
        url: 'http://localhost:9999/railways/station/get/by/id',
        dataType: 'JSON',
        data: {
            stationId: stationId
        },
        success: function (data) {

            document.getElementById("nStation").value = data.name;
        },
        error: function (data) {
        }
    });
}

function getVocabulary() {
    return {
        ru: {
            'edClient': 'Изменить клиента',
            'fName': 'Имя',
            'lName': 'Фамилия',
            'phNumber': 'Телефон',
            'sClient': 'Сохранить',
            'dClient': 'Удалить',
            'cancel' : 'Отмена',
            'fillUp' : 'Заполните все поля.',
            'exists' : 'Станция уже в базе.'

        },
        en: {
            'edClient': 'Edit station',
            'fName': 'First name',
            'lName': 'Last name',
            'phNumber': 'Phone number',
            'sClient': 'Save station',
            'dClient': 'Delete station',
            'cancel' : 'Cancel',
            'fillUp' : 'Please fill up all the fields.',
            'exists' : 'Station already exists'
        }

    };
}

function translatePage(transLang) {

    $('.lang').each(function (index, element) {
        $(this).text(vocabulary[transLang][$(this).attr('key')]);
    });

}

function updateStation(data) {
    console.log('Update start');
    $.ajax({
        type: 'post',
        url: 'http://localhost:9999/railways/station/update',
        dataType: 'JSON',
        data: {
            jsonStation: data
        },
        success: function (success) {

        },
        error: function (response) {
            if (response.status === 406) {
                alert(vocabulary[language]['exists']);
                location.reload();
                console.log('Update finish')
            }
        },
        complete: function (response) {
            if (response.status === 200) {
                $(location).attr('href', 'http://localhost:9999/html/station/StationsList.html');
            }
        }
    });
}